/*
 * Copyright 2013 Alessandro Atria - a.atria@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.atria.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Consumer;

import org.atria.dao.util.SqlScript;

public enum DAO implements IDAO {
	Sqlite;
	
	private String connetionUrl;
	private Connection conn = null;
	private final static String INNER_DB = "INNER_DB";
	private static final Map<String, Map<String, Object>> cached_objects = new TreeMap<>(); 
	private static final Map<String, List<Class<?>>> cached_classes = new TreeMap<>();
	private static final Map<String, Connection> databases = new HashMap<>();
	
	public void use(Connection conn) throws Exception{
		this.conn = conn;
	}
	
	public void use() throws Exception {
		use("memory");
	}
	
	public void use(File dbFile) throws Exception {
		if (dbFile == null || !dbFile.exists()){
			throw new FileNotFoundException();
		}
		String dbName = dbFile.getName();
		if(databases.containsKey(dbName)){
			conn = databases.get(dbName);
		} else {
			Class.forName("org.sqlite.JDBC");
			System.out.println("DAO.Sqlite() "+dbFile.getAbsolutePath());
			this.connetionUrl = "jdbc:sqlite:"+dbFile.getAbsolutePath();
			conn = DriverManager.getConnection(connetionUrl);
			databases.put(dbName, conn);
		}
	}
	
	public void use(String dbFile) throws Exception {
		if (dbFile == null || dbFile.equals("")){
			dbFile = "memory";
		}
		
		if(databases.containsKey(dbFile)){
			conn = databases.get(dbFile);
		} else {
			Class.forName("org.sqlite.JDBC");
			if(dbFile.equals("memory")){
				this.connetionUrl = "jdbc:sqlite::memory:";
			} else {
				File pwd = new File("./");
				System.out.println("DAO.Sqlite() "+pwd.getAbsolutePath());
				this.connetionUrl = "jdbc:sqlite:"+pwd.getAbsolutePath()+File.separator+dbFile;
			}
			conn = DriverManager.getConnection(connetionUrl);
			databases.put(dbFile, conn);
		}
	}
	
	private boolean checkEntityTable(Connection conn, Class<?> entity) throws Exception {
		if(conn == null || conn.isClosed())
			throw new Exception("Not connected");
		if(!DAOReflect.isEntity(entity)){
			throw new Exception("Object passed as parameter is not an Entity");
		}
		if(!cached_classes.containsKey(INNER_DB)){
			cached_classes.put(INNER_DB, new ArrayList<Class<?>>());
		}
		if(!cached_classes.get(INNER_DB).contains(entity)){
			cached_classes.get(INNER_DB).add(entity);
			executeNativeSQL(DAOReflect.sqlTableScript(DAOReflect.getEntity(entity)));
		}
		IEntity a = entity.getAnnotation(IEntity.class);
		if(a != null){
			return a.cache();
		}
		return false;
	}
	
	public void disconnect() throws Exception{
		if(conn != null && !conn.isClosed())
			conn.close();
	}
	
	@Override
	public void beginTrans() throws SQLException{
		if(conn != null && !conn.isClosed()){
			conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			conn.setAutoCommit(false);
		}
	}
	
	@Override
	public void rollbackTrans() throws SQLException{
		try{
			if(conn != null && !conn.isClosed()){
				conn.rollback();
			}
		}finally{
			conn.setAutoCommit(true);
			conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		}
	}
	
	@Override
	public void commitTrans() throws SQLException{
		try{
			if(conn != null && !conn.isClosed()){
				conn.commit();
			}
		}finally{
			conn.setAutoCommit(true);
			conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#load(java.lang.Class, java.lang.Object[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T load(Class<T> clazz, Object... values) throws Exception {
		boolean cacheable = checkEntityTable(conn, clazz);
		if(cacheable && cached_objects.get(clazz.getName()) != null){
			T res = (T)cached_objects.get(clazz.getName()).get(DAOReflect.asString( values ));
			if(res != null){
				return res;
			}
		}
		try{
			Object entity = clazz.newInstance();
			Field[] pk = DAOReflect.pk(entity);
			for (int i = 0; i < pk.length; i++) {
				DAOReflect.set(entity, pk[i], values[i]);
			}
			Object res = get(entity);
			if(cacheable && res != null){
				if(cached_objects.get(clazz.getName()) == null){
					cached_objects.put(clazz.getName(), new ConcurrentSkipListMap<String, Object>());
				}
				cached_objects.get(clazz.getName()).put(DAOReflect.asString( values ), res);
			}
			return (T) res;
		}catch(Exception e){
			System.err.println("Error on"+ clazz.getName());
			e.printStackTrace();
			throw e;
		}
	}
	
	private void setField(final Field field, final Object entity, final ResultSet res) throws Exception{
		if(field.getType().equals(String.class)){
			DAOReflect.set(entity, field, res.getString(DAOReflect.getName(field)));
		} else if(field.getType().equals(java.util.Date.class)){
			DAOReflect.set(entity, field, res.getDate(DAOReflect.getName(field)));
		} else if(field.getType().equals(double.class)){
			DAOReflect.set(entity, field, new Double(res.getDouble(DAOReflect.getName(field))));
		} else if(field.getType().equals(float.class)){
			DAOReflect.set(entity, field, new Float(res.getFloat(DAOReflect.getName(field))));
		} else if(field.getType().equals(boolean.class)){
			DAOReflect.set(entity, field, new Boolean(res.getBoolean(DAOReflect.getName(field))));
		} else if(field.getType().equals(int.class)){
			DAOReflect.set(entity, field, new Integer(res.getInt(DAOReflect.getName(field))));
		} else if(field.getType().equals(short.class)){
			DAOReflect.set(entity, field, new Short(res.getShort(DAOReflect.getName(field))));
		} else if(field.getType().equals(long.class)){
			DAOReflect.set(entity, field, new Long(res.getLong(DAOReflect.getName(field))));
		} else {
			System.out.println("\t-->"+DAOReflect.getName(field));
			DAOReflect.set(entity, field, res.getString(DAOReflect.getName(field)));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#get(com.atria.persister.IEntity)
	 */
	@Override
	public <T> T get(T entity) throws Exception {
		checkEntityTable(conn, entity.getClass());
		PreparedStatement pstmt = null;
		ResultSet res = null;
		try {
			pstmt = sqlSelect(conn, entity);
			
			res = pstmt.executeQuery();
			Field[] fields = DAOReflect.fields(entity, true); // entity.getClass().getDeclaredFields();
			
			if(res.next()){
				for (int i = 0; i < fields.length; i++) {
					setField(fields[i], entity, res);
					IField f = fields[i].getAnnotation(IField.class);
					if(f != null && !f.fk().equals(Object.class)){
						Object o = load(f.fk() , DAOReflect.get(entity, fields[i]));
						DAOReflect.load(entity, o.getClass().getSimpleName(), o);
					}
				}
				return entity;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			DAOReflect.logObject(entity);
			//log.error("get", e);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	private void closeAll(PreparedStatement pstmt, ResultSet res) {
		try {
			pstmt.close();
		} catch (Exception e) {}
		try {
			res.close();
		} catch (Exception e) {}
	}

	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#update(com.atria.persister.IEntity)
	 */
	@Override
	public <T> int update(T entity) throws Exception {
		boolean cacheble = checkEntityTable(conn, entity.getClass());
		if(cacheble && cached_objects.get(entity.getClass().getName()) != null){
			cached_objects.get(entity.getClass().getName()).remove(DAOReflect.asString( DAOReflect.pkValues(entity) ));
		}
		PreparedStatement pstmt = null;
		try{
			pstmt = sqlUpdate(conn, entity);
			return pstmt.executeUpdate();
		} catch (Exception e) {
			DAOReflect.logObject(entity);
			//log.error("update", e);
			throw e;
		} finally {
			closeAll(pstmt);
		}
	}
	
	@Override
	public <T> int delete(T entity) throws Exception {
		boolean cacheble = checkEntityTable(conn, entity.getClass());
		if(cacheble && cached_objects.get(entity.getClass().getName()) != null){
			cached_objects.get(entity.getClass().getName()).remove(DAOReflect.asString( DAOReflect.pkValues(entity) ));
		}
		PreparedStatement pstmt = null;
		try{
			pstmt = sqlDelete(conn, entity);
			return pstmt.executeUpdate();
		} catch (Exception e) {
			DAOReflect.logObject(entity);
			//log.error("update", e);
			throw e;
		} finally {
			closeAll(pstmt);
		}
	}
	
	@Override
	public <T> int update(T entity, String...exclude) throws Exception {
		boolean cacheble = checkEntityTable(conn, entity.getClass());
		if(cacheble && cached_objects.get(entity.getClass().getName()) != null){
			cached_objects.get(entity.getClass().getName()).remove(DAOReflect.asString( DAOReflect.pkValues(entity) ));
		}
		PreparedStatement pstmt = null;
		try{
			pstmt = sqlUpdate(conn, entity, exclude);
			return pstmt.executeUpdate();
		} catch (Exception e) {
			DAOReflect.logObject(entity);
			//log.error("update", e);
			throw e;
		} finally {
			closeAll(pstmt);
		}
	}
	
	private void closeAll(PreparedStatement pstmt) {
		try {
			pstmt.close();
		} catch (Exception e) {}
		
	}
	
	private void closeAll(Statement pstmt) {
		try {
			pstmt.close();
		} catch (Exception e) {}
	}

	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#insert(com.atria.persister.IEntity)
	 */
	@Override
	public int insert(Object entity) throws Exception {
		if(entity == null) return -1;
		checkEntityTable(conn, entity.getClass());
		PreparedStatement pstmt = null;
		try{
			pstmt = sqlInsert(conn, entity);
			return pstmt.executeUpdate();
		} catch (Exception e) {
			DAOReflect.logObject(entity);
			throw e;
		} finally {
			closeAll(pstmt);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#select(com.atria.persister.IEntity)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> select(T entity) throws Exception {
		checkEntityTable(conn, entity.getClass());
		PreparedStatement pstmt = null;
		ResultSet res = null;
		List<T> result = new ArrayList<T>();
		try {
			pstmt = sqlSelectByEquals(conn, entity);
			res = pstmt.executeQuery();
			Field[] fields = DAOReflect.fields(entity, true); 
			Object obj = null;
			while(res.next()){
				obj = entity.getClass().newInstance();
				for (int i = 0; i < fields.length; i++) {
					setField(fields[i], obj, res);
					IField f = fields[i].getAnnotation(IField.class);
					if(f != null && !f.fk().equals(Object.class)){
						Object o = load(f.fk() , DAOReflect.get(obj, fields[i]));
						DAOReflect.load(obj, o.getClass().getSimpleName(), o);
					}
				}
				result.add((T)obj);
			}
			return result;
		} catch (Exception e) {
			DAOReflect.logObject(entity);
			//log.error("select", e);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#delete(com.atria.persister.Criteria)
	 */
	@Override
	public <T> int delete(Criteria<T> criteria) throws Exception {
		boolean cacheable = checkEntityTable(conn, criteria.clazz);
		if(cacheable){
			cached_objects.remove(criteria.clazz.getName());
		}
		int result = 0;
		PreparedStatement pstmt = null;
		try {
			pstmt = sqlDeleteByCriteria(conn, criteria);
			result = pstmt.executeUpdate();
			return result;
		} catch (Exception e) {
			DAOReflect.logObject(criteria);
			throw e;
		} finally {
			closeAll(pstmt);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#select(com.atria.persister.Criteria)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> select(Criteria<T> criteria) throws Exception {
		checkEntityTable(conn, criteria.clazz);
		PreparedStatement pstmt = null;
		ResultSet res = null;
		List<T> result = new ArrayList<T>();
		try {
			pstmt = sqlSelectByCriteria(conn, criteria);
			Object entity = criteria.getIEntity();
			res = pstmt.executeQuery();
			Field[] fields = DAOReflect.fields(entity, true); //entity.getClass().getDeclaredFields();
			Object obj = null;
			while(res.next()){
				obj = entity.getClass().newInstance();
				for (int i = 0; i < fields.length; i++) {
					//System.out.println(fields[i].getName());
					setField(fields[i], obj, res);
					IField f = fields[i].getAnnotation(IField.class);
					if(f != null && !f.fk().equals(Object.class)){
						Object o = load(f.fk() , DAOReflect.get(obj, fields[i]));
						DAOReflect.load(obj, o.getClass().getSimpleName(), o);
					}
				}
				result.add((T)obj);
			}
			return result;
		} catch (Exception e) {
			DAOReflect.logObject(criteria);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T>  T selectFirst(Criteria<T> criteria) throws Exception {
		checkEntityTable(conn, criteria.clazz);
		PreparedStatement pstmt = null;
		ResultSet res = null;
//		List<T> result = new ArrayList<T>();
		try {
			pstmt = sqlSelectByCriteria(conn, criteria);
			Object entity = criteria.getIEntity();
			res = pstmt.executeQuery();
			Field[] fields = DAOReflect.fields(entity, true); //entity.getClass().getDeclaredFields();
			Object obj = null;
			if(res.next()){
				obj = entity.getClass().newInstance();
				for (int i = 0; i < fields.length; i++) {
					//System.out.println(fields[i].getName());
					setField(fields[i], obj, res);
					IField f = fields[i].getAnnotation(IField.class);
					if(f != null && !f.fk().equals(Object.class)){
						Object o = load(f.fk() , DAOReflect.get(obj, fields[i]));
						DAOReflect.load(obj, o.getClass().getSimpleName(), o);
					}
				}
				//result.add((T)obj);
			}
			return (T)obj;
		} catch (Exception e) {
			DAOReflect.logObject(criteria);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> int select(Criteria<T> criteria, Consumer<T> callback) throws Exception {
		checkEntityTable(conn, criteria.clazz);
		PreparedStatement pstmt = null;
		ResultSet res = null;
		try {
			pstmt = sqlSelectByCriteria(conn, criteria);
			Object entity = criteria.getIEntity();
			res = pstmt.executeQuery();
			Field[] fields = DAOReflect.fields(entity, true); //entity.getClass().getDeclaredFields();
			Object obj = entity.getClass().newInstance();
			int processedRows = 0;
			while(res.next()){
				for (int i = 0; i < fields.length; i++) {
					setField(fields[i], obj, res);
					final IField f = fields[i].getAnnotation(IField.class);
					if(f != null && !f.fk().equals(Object.class)){
						Object o = load(f.fk() , DAOReflect.get(obj, fields[i]));
						DAOReflect.load(obj, o.getClass().getSimpleName(), o);
					}
				}
				callback.accept((T)obj);
				processedRows++;
			}
			return processedRows;
		} catch (Exception e) {
			DAOReflect.logObject(criteria);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	public <T extends Number> T number(Class<T> clazz, String sql, Object...values) throws Exception{
		PreparedStatement pstmt = null;
		ResultSet res = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int idx = 1;
			for(Object v:values){
				pstmt.setObject(idx++, v);
			}
			res = pstmt.executeQuery();
			if(res.next()){
				if(clazz.equals(Double.class)){
					return clazz.getConstructor(double.class).newInstance(res.getDouble(1));
				} else if(clazz.equals(Float.class)){
					return clazz.getConstructor(float.class).newInstance(res.getFloat(1));
				} else if(clazz.equals(Integer.class)){
					return clazz.getConstructor(int.class).newInstance(res.getInt(1));
				} else if(clazz.equals(Long.class)){
					return clazz.getConstructor(long.class).newInstance(res.getLong(1));
				} else if(clazz.equals(Short.class)){
					return clazz.getConstructor(short.class).newInstance(res.getShort(1));
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	public <T> List<T> select(Class<T> clazz, String sql, Object...values) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet res = null;
		List<T> result = new ArrayList<T>();
		try {
			pstmt = conn.prepareStatement(sql);
			int idx = 1;
			for(Object v:values){
				pstmt.setObject(idx++, v);
			}
			
			res = pstmt.executeQuery();
			T obj = clazz.newInstance();
			Field[] fields = DAOReflect.fields(obj, true); //entity.getClass().getDeclaredFields();
			while(res.next()){
				obj = clazz.newInstance();
				for (int i = 0; i < fields.length; i++) {
					setField(fields[i], obj, res);
				}
				result.add((T)obj);
			}
			return result;
		} catch (Exception e) {
			//DAOUtils.logObject(criteria);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	public <T> T selectFirst(Class<T> clazz, String sql, Object...values) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet res = null;
		try {
			pstmt = conn.prepareStatement(sql);
			int idx = 1;
			for(Object v:values){
				pstmt.setObject(idx++, v);
			}
			
			res = pstmt.executeQuery();
			T obj = clazz.newInstance();
			Field[] fields = DAOReflect.fields(obj, true); //entity.getClass().getDeclaredFields();
			if(res.next()){
				obj = clazz.newInstance();
				for (int i = 0; i < fields.length; i++) {
					setField(fields[i], obj, res);
				}
				return obj;
			}
			return null;
		} catch (Exception e) {
			//DAOUtils.logObject(criteria);
			throw e;
		} finally {
			closeAll(pstmt, res);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.atria.persister.IPersister#save(com.atria.persister.IEntity)
	 */
	@Override
	public boolean save(Object entity)throws Exception{
		checkEntityTable(conn, entity.getClass());
		if(update(entity) == 0){
			insert(entity);
			return true;
		}
		return false;
	}
	
	
	@Override
	public <T> boolean save(T entity, Criteria<T> criteria) throws Exception{
		checkEntityTable(conn, entity.getClass());
		if(!exists(criteria)){
			insert(entity);
			return true;
		} else {
			
			return false;
		}
	}
	
	@Override
	public <T> boolean exists(Criteria<T> criteria) throws Exception {
		checkEntityTable(conn, criteria.clazz);
		return select(criteria).size() > 0;
	}
	
	@Override
	public <T> T first(Criteria<T> criteria) throws Exception {
		checkEntityTable(conn, criteria.clazz);
		criteria.limit(1);
//		conn.getClientInfo().forEach( (a, b)-> System.out.println("DAO.first() "+String.valueOf(a)+" - "+String.valueOf(b)) );
		List<T> list = select(criteria);
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	@Override
	public <T> T last(Criteria<T> criteria) throws Exception {
		checkEntityTable(conn, criteria.clazz);
		List<T> list = select(criteria);
		if(list.size() > 0)
			return list.get(list.size()-1);
		else
			return null;
	}
	
	
	private PreparedStatement sqlSelect(Connection con, Object entity) throws Exception {
		StringBuilder buffer = new StringBuilder("select * ");
		buffer.append(" from ");
		buffer.append(DAOReflect.getTableName(entity));
		buffer.append(" where 1 = 1 ");
		Field[] pk = DAOReflect.pk(entity);
		for (int i = 0; i < pk.length; i++) {
			buffer.append(" and ");
			buffer.append(DAOReflect.getName(pk[i]));
			buffer.append(" = ? ");
		}
		
		PreparedStatement pstmt = con.prepareStatement(buffer.toString());
		for (int i = 0; i < pk.length; i++) {
			pstmt.setObject(i+1, DAOReflect.get(entity, pk[i]));
		}
		return pstmt;
	}
	
	private PreparedStatement sqlSelectByEquals(Connection con, Object entity) throws Exception {
		StringBuilder buffer = new StringBuilder("select ");
		Field[] fields = DAOReflect.fields(entity, true); //entity.getClass().getDeclaredFields();
		
		Map<Field, Object> searchData = new HashMap<Field, Object>();
		Object appo = null;
		for (int i = 0; i < fields.length; i++) {
			if(i != 0)
				buffer.append(", ");
			
			buffer.append(DAOReflect.getName(fields[i]));
			appo = DAOReflect.get(entity, fields[i]);
			if(appo != null){
				if(appo instanceof Long && ((Long)appo).longValue() == 0){
					//do nothing
				} else {
					searchData.put(fields[i], appo);
				}
			}
		}
		buffer.append(" from ");
		buffer.append(DAOReflect.getTableName(entity));
		buffer.append(" where 1 = 1 ");
		Field tmp;
		for(Iterator<Field> iterator = searchData.keySet().iterator(); iterator.hasNext();){
			tmp = iterator.next();
			buffer.append(" and ");
			buffer.append(DAOReflect.getName(tmp));
			buffer.append(" = ? ");
		}
		
		PreparedStatement pstmt = con.prepareStatement(buffer.toString());
		int idx = 1;
		for(Iterator<Field> iterator = searchData.keySet().iterator(); iterator.hasNext();){
			tmp = iterator.next();
			pstmt.setObject(idx++, DAOReflect.get(entity, tmp));
		}
		
		return pstmt;
	}
	
	
	private <T> PreparedStatement sqlSelectByCriteria(Connection con, Criteria<T> criteria) throws Exception {
		StringBuilder buffer = new StringBuilder("select ");
		
		if(criteria.distinct.length == 0){
			buffer.append(" * ");
		} else {
			buffer.append(" distinct ");
			for(int i = 0; i < criteria.distinct.length; i++){
				if(i>0){
					buffer.append(", ");
				}
				buffer.append(criteria.distinct[i]);
				
			}
		}
		Object entity = criteria.getIEntity();
		
		buffer.append(" from ");
		buffer.append(DAOReflect.getTableName(entity));
		buffer.append(" where 1 = 1 ");
		
		List<Where> searchCriteria =  criteria.getCriteria();
		List<Object> searchData = new ArrayList<Object>();
		Where where = null;
		for (Iterator<Where> iterator = searchCriteria.iterator(); iterator.hasNext();) {
			where = iterator.next();
			buffer.append(where.toString());
			if(where.operator != Where.IS_NULL && where.operator != Where.IS_NOT_NULL){
				Collections.addAll(searchData, where.values);
			}
		}
		if(criteria.groupBy != null && criteria.groupBy.length > 0){
			buffer.append(" group by ");
			for (int i = 0; i < criteria.groupBy.length ; i++) {
				if(i != 0) buffer.append(", ");
				
				buffer.append(criteria.groupBy[i]);
			}
		}
		if(criteria.orderBy != null && criteria.orderBy.length > 0){
			buffer.append(" order by ");
			for (int i = 0; i < criteria.orderBy.length ; i++) {
				if(i != 0) buffer.append(", ");
				
				buffer.append(criteria.orderBy[i]);
			}
		}
		if(criteria.rows > 0){
			buffer.append(" limit ");
			buffer.append(criteria.rows);
		}
//		System.out.println("DAO.sqlSelectByCriteria() "+buffer.toString());
		PreparedStatement pstmt = con.prepareStatement(buffer.toString());
		int idx = 1;
		Object tmp;
		for(Iterator<Object> iterator = searchData.iterator(); iterator.hasNext();){
			tmp = iterator.next();
			pstmt.setObject(idx++, tmp);
		}
		
		return pstmt;
	}
	
	private <T> PreparedStatement sqlDeleteByCriteria(Connection con, Criteria<T> criteria) throws Exception {
		Object entity = criteria.getIEntity();
		StringBuilder buffer = new StringBuilder("delete from ");
		buffer.append(DAOReflect.getTableName(entity));
		buffer.append(" where 1 = 1 ");
		
		List<Where> deleteCriteria =  criteria.getCriteria();
		List<Object> deleteData = new ArrayList<Object>();
		Where where = null;
		for (Iterator<Where> iterator = deleteCriteria.iterator(); iterator.hasNext();) {
			where = iterator.next();
			buffer.append(where.toString());
			if(where.operator != Where.IS_NULL && where.operator != Where.IS_NOT_NULL){
				Collections.addAll(deleteData, where.values);
			}
		}
		PreparedStatement pstmt = con.prepareStatement(buffer.toString());
		int idx = 1;
		Object tmp;
		for(Iterator<Object> iterator = deleteData.iterator(); iterator.hasNext();){
			tmp = iterator.next();
			pstmt.setObject(idx++, tmp);
		}
		
		return pstmt;
	}
	
	private PreparedStatement sqlUpdate(Connection con, Object entity) throws Exception {
		return sqlUpdate(con, entity, new String[0]);
	}
	
	private PreparedStatement sqlDelete(Connection con, Object entity) throws Exception {
		StringBuilder buffer = new StringBuilder("delete from ");
		buffer.append(DAOReflect.getTableName(entity));
		buffer.append(" where 1 = 1 ");
		Field[] pk = DAOReflect.pk(entity);
		for (int i = 0; i < pk.length; i++) {
			buffer.append(" and ");
			buffer.append(DAOReflect.getName(pk[i]));
			buffer.append(" = ? ");
		}
		PreparedStatement pstmt = con.prepareStatement(buffer.toString());
		int idx = 1;
		for (int i = 0; i < pk.length; i++) {
			pstmt.setObject(idx++, DAOReflect.get(entity, pk[i]));
		}
		return pstmt;
	}
	
	private PreparedStatement sqlUpdate(Connection con, Object entity, String[] exclude) throws Exception {
		StringBuilder buffer = new StringBuilder("update ");
		buffer.append(DAOReflect.getTableName(entity));
		buffer.append(" set ");
		Field[] fields = DAOReflect.fields(entity, true); //entity.getClass().getDeclaredFields();
		String fieldName = null;
		boolean comma = false;
		for (int i = 0; i < fields.length; i++) {
			fieldName = DAOReflect.getName(fields[i]);
			if(!contains(exclude, fieldName)){
				if(comma)
					buffer.append(", ");
				buffer.append(fieldName);
				buffer.append(" = ? ");
				comma = true;
			}
		}
		buffer.append(" where 1 = 1 ");
		Field[] pk = DAOReflect.pk(entity);
		for (int i = 0; i < pk.length; i++) {
			buffer.append(" and ");
			buffer.append(DAOReflect.getName(pk[i]));
			buffer.append(" = ? ");
		}
		
		PreparedStatement pstmt = con.prepareStatement(buffer.toString());
		int idx = 1;
		Object obj;
		for (int i = 0; i < fields.length; i++) {
			if(!contains(exclude, fields[i].getName())){
				obj = DAOReflect.get(entity, fields[i]);
				//System.out.println(obj);
				if(obj != null){
					if(obj.getClass().equals(Boolean.class)){
						pstmt.setObject(idx++, ((Boolean)obj)?1:0);
					} else {
						pstmt.setObject(idx++, obj);
					}
				} else {
					if(fields[i].getType().equals(String.class)){
						try{
							pstmt.setNull(idx++, Types.CHAR);
						}catch(Exception e){
							pstmt.setNull(idx, Types.VARCHAR);
						}
					} else if(fields[i].getType().equals(java.util.Date.class)){
						pstmt.setNull(idx++, Types.DATE);
					} else if(fields[i].getType().equals(double.class)){
						pstmt.setNull(idx++, Types.DOUBLE);
					} else if(fields[i].getType().equals(boolean.class)){
						pstmt.setNull(idx++, Types.BOOLEAN);
					} else {
						pstmt.setNull(idx++, Types.NUMERIC);
					}
				}				
			}
		}
		//System.out.println("------");
		for (int i = 0; i < pk.length; i++) {
			pstmt.setObject(idx++, DAOReflect.get(entity, pk[i]));
		}
		return pstmt;
	}
	
	private boolean contains(String[] input, String match){
		for (int i = 0; i < input.length; i++) {
			if(input[i].equals(match)){
				return true;
			}
		}
		return false;
	}
	
	private PreparedStatement sqlInsert(Connection con, Object entity) throws Exception {
		StringBuilder ins = new StringBuilder("insert into ");
		StringBuilder values = new StringBuilder(" values ( ");
		ins.append(DAOReflect.getTableName(entity));
		
		ins.append(" ( ");
		Field[] fields = DAOReflect.fields(entity, true); //entity.getClass().getDeclaredFields();
		boolean commaSep = false;
		List<Object> insertData = new ArrayList<Object>();
		for (int i = 0; i < fields.length; i++) {
			if(!DAOReflect.isSerial(fields[i])){
				if(commaSep){
					ins.append(", ");
					values.append(", ");
				}
				
				ins.append(DAOReflect.getName(fields[i]));
				values.append(" ? ");
				commaSep = true;
				insertData.add(DAOReflect.get(entity, fields[i]));
			}
		}
		ins.append(" ) ");
		values.append(" ) ");
		//System.out.println(ins.toString()+values.toString());
		PreparedStatement pstmt = con.prepareStatement(ins.toString()+values.toString());
		int idx = 1; 
		for (int i = 0; i < insertData.size(); i++) {
			if(insertData.get(i) instanceof Boolean){
				pstmt.setObject(idx++, ((Boolean)insertData.get(i))?1:0);
			} else {
				pstmt.setObject(idx++, insertData.get(i));
			}
		}
		return pstmt;
	}
	
	@Override
	public void executeNativeSQL(String sql, Object...values) throws Exception {
		PreparedStatement pstmt = null;
//		System.out.println(sql);
		try{
			pstmt = conn.prepareStatement(sql);
			for(int i = 1; i <= values.length; i++){
				pstmt.setObject(i, values[i-1]);
			}
			pstmt.execute();
		}catch(Exception e){
			throw e;
		} finally{
			 closeAll(pstmt);
		}
	}
	
	
	
	@Override
	public void executeNativeSQL(String sql) throws Exception {
		Statement pstmt = null;
//		System.out.println(sql);
		try{
			pstmt = conn.createStatement();
			String[] statements = sql.split(";");
			for(String s:statements){
				pstmt.addBatch(s);
			}
			pstmt.executeBatch();
		}catch(Exception e){
			throw e;
		} finally{
			 closeAll(pstmt);
		}
	}
	
	@Override
	public List<Map<String, Object>> executeNativeSQLQuery(String sql, Object...values) throws Exception {
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		PreparedStatement pstmt = null;
		ResultSetMetaData meta = null;
		ResultSet res = null;
		try{
			pstmt = conn.prepareStatement(sql);
			for(int i = 1; i <= values.length; i++){
				pstmt.setObject(i, values[i-1]);
			}
			
			meta = pstmt.getMetaData();
			final int size = meta.getColumnCount();
			res = pstmt.executeQuery();
			Map<String, Object> row;
			
			while(res.next()){
				row = new HashMap<String, Object>();
				for(int i = 1 ; i <= size; i++){
					row.put(meta.getColumnName(i), res.getObject(i));
				}
				result.add(row);
			}
			return result;
		}catch(Exception e){
			throw e;
		} finally{
			 closeAll(pstmt, res);
		}
	}
	
	public void createTableIfNotExist(Class<?> clazz) throws Exception{
		executeNativeSQL(DAOReflect.sqlTableScript(clazz));
	}
	
	@Override
	public Connection getConnection() {
		return conn;
	}
	
	public boolean loadSqlScript(final File file){
		BufferedReader reader = null;
		try{
			reader = new BufferedReader( new FileReader(file));
			SqlScript sqlScript = new SqlScript(reader, getConnection());
			sqlScript.loadScript();
			sqlScript.execute();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
