/*
 * Copyright 2013 Alessandro Atria - a.atria@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.atria.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * This interface represents the core of the library and exposes all usable methods.
 * 
 * @author Alessandro Atria - a.atria@gmail.com
 */
public interface IDAO {
	
	/**
	 * Connect or switch to memory database
	 * @throws Exception
	 */
	public void use() throws Exception;
	
	/**
	 * Connect or switch to file database. if dbFile is null or blank memory database i connected
	 * @param dbFile
	 * @throws Exception
	 */
	public void use(String dbFile) throws Exception;
	
	/**
	 * Retrieves the instance of the class passed as parameter and identified by one or more primary key values
	 * @param clazz Entity class
	 * @param values one or more primary key values, if there are more than one primary key pass the parameters using the same order with which they are defined in entity class
	 * @return the instance of the class clazz, null if passed primary key values don't match occurrences.
	 * @throws Exception
	 */
	public <T> T load(Class<T> clazz, Object... values) throws Exception;

	/**
	 * Retrieves the instance passed as parameter (with valued primary key fields) with all data, or null if the primary key values don't match occorrences 
	 * @param entity
	 * @return entity instance 
	 * @throws Exception
	 */
	public <T> T get(T obj) throws Exception;

	/**
	 * Esegue l'aggiornamento dell'istanza passata come parametro in input
	 * della quale devono essere necessariamente valorizzati i/il campo primary key.
	 * L'aggiornamento avviene per tutti i campi della della classe � pertanto consigliato
	 * fare prima la get o la load dell'istanza che si vuole modificare e poi eseguire la modifica
	 * dei soli campi oggetto di aggiornamento
	 * @param entity
	 * @return numero delle occorrenze aggirnate in banca dati
	 * @throws Exception
	 */
	public <T> int update(T entity) throws Exception;
	
	
	public <T> int update(T entity, String...exclude) throws Exception;
	
	public <T> boolean save(T entity, Criteria<T> criteria) throws Exception;
	
	public <T> boolean exists(Criteria<T> criteria) throws Exception;
	
	public <T> T first(Criteria<T> criteria) throws Exception;
	
	public <T> T last(Criteria<T> criteria) throws Exception;
	
	public <T> int delete(T entity) throws Exception;
	
	public <T> int delete(Criteria<T> criteria) throws Exception;

	/**
	 * Esegue l'inserimento in banca dati dell'oggetto passato come parametro.
	 * Nel caso in cui la tabella sia di tipo BDN la chiave primaria deve essere impostata 
	 * sull'oggetto in input, per le tabelle tipo BDU che hanno le sequence quest'ultima viene invocata
	 * automanticamente in base allo short name della tabella.
	 * @param entity
	 * @return 
	 * @throws Exception
	 */
	public <T> int insert(T entity) throws Exception;
	

	/**
	 * Esegue la query sull'entit� passata come parametro in input e va in equals per tutti i campi che 
	 * non sono null.
	 * @param entity
	 * @return List di oggetti di tipo entity per la tabella oggetto di interesse
	 * @throws Exception
	 */
	public <T> List<T> select(T entity) throws Exception;

	/**
	 * Esegue la ricerca in base ai criteri impostati nel parametro in input
	 * @param <T>
	 * @param criteria
	 * @return List di oggetti per il criterio passato in input
	 * @throws Exception
	 */
	public <T> List<T> select(Criteria<T> criteria) throws Exception;
	
	public <T>  T selectFirst(Criteria<T> criteria) throws Exception;
	
	public <T> int select(Criteria<T> criteria, Consumer<T> callback) throws Exception;
	
	public <T> List<T> select(Class<T> clazz, String sql, Object...values) throws Exception;
	
	public <T> T selectFirst(Class<T> clazz, String sql, Object...values) throws Exception;
	
	public <T extends Number> T number(Class<T> clazz, String sql, Object...values) throws Exception;
	
	/**
	 * Il metodo provvede al salvataggio dell'entit� sul data base
	 * a prescindere se l'entit� in oggetto gi� esiste oppure � di nuova creazione
	 * @param entity
	 * @return true se viene inserito, false in caso di update
	 * @throws Exception
	 */
	public <T> boolean save(T entity) throws Exception;
	
	public void executeNativeSQL(String sql, Object...values) throws Exception;
	
	public void executeNativeSQL(String sql) throws Exception;
	
	public List<Map<String, Object>> executeNativeSQLQuery(String sql, Object...values) throws Exception;
	
	public void beginTrans() throws SQLException;
	
	public void rollbackTrans() throws SQLException;
	
	public void commitTrans() throws SQLException;
	
	public Connection getConnection();
	
	public boolean loadSqlScript(final File file);
	
	public void createTableIfNotExist(Class<?> entity) throws Exception;
}