/*
 * Copyright 2013 Alessandro Atria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.atria.dao.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DoubleKeyMap {
	private List<Object[]> pairs = new ArrayList<Object[]>();
	public DoubleKeyMap(){
	}
	
	public void put(Object key1, Object key2){
		pairs.add(new Object[]{key1, key2, null});
	}
	
	public void put(Object key1, Object key2, Object value){
		pairs.add(new Object[]{key1, key2, value});
	}
	
	@SuppressWarnings("rawtypes")
	public Object getAlterKey(Object key){
		Object[] pair = null;
		for (Iterator iterator = pairs.iterator(); iterator.hasNext();) {
			pair = (Object[]) iterator.next();
			if(key.equals(pair[0])){
				return pair[1];
			} else if(key.equals(pair[1])){
				return pair[0];
			}
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	public Object getValue(Object key){
		Object[] pair = null;
		for (Iterator iterator = pairs.iterator(); iterator.hasNext();) {
			pair = (Object[]) iterator.next();
			if(key.equals(pair[0])){
				return pair[2];
			} else if(key.equals(pair[1])){
				return pair[2];
			}
		}
		return null;
	}
	
	public Object[] getPairsByValue(final Object value){
		for(Object[] v:pairs){
			if(v.length == 3 && v[2] != null && v[2].equals(value)){
				return v;
			}
		}
		return null;
	}
	
}
