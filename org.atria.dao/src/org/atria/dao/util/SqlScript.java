/*
 * Copyright 2013 Alessandro Atria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.atria.dao.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlScript {

	public final static char QUERY_ENDS = ';';

	private Connection conn;
	private BufferedReader rdr;
	private Statement statement;

	/**
	 * Constructor: takes a bufferedReader and a sql connection to create the SqlScript object.
	 * Note that construction does not automatically read the script.
	 * @param bufRdr BufferedReader to the script data.
	 * @param connection SQL Connection
	 * @throws SQLException
	 */
	public SqlScript(BufferedReader bufRdr, Connection connection) throws SQLException {
		rdr = bufRdr;
		conn = connection;
		statement = conn.createStatement();
	}

	/**
	 * Loads the Sql Script from the BufferedReader and parses it into a statement.
	 * @throws IOException
	 * @throws SQLException
	 */
	public void loadScript() throws IOException, SQLException {
		String line;
		StringBuilder query = new StringBuilder();
		boolean queryEnds = false;

		while ((line = rdr.readLine()) != null) {
			if (isComment(line))
				continue;
			queryEnds = checkStatementEnds(line);
			query.append(line);
			if (queryEnds) {
				statement.addBatch(query.toString());
				query.setLength(0);
			}
		}
	}

	/**
	 * @param line
	 * @return
	 */
	private boolean isComment(String line) {
		if ((line != null) && (line.length() > 0))
			return (line.charAt(0) == '#');
		return false;
	}

	/**
	 * Executes the statement created by loadScript.
	 *
	 * @throws IOException
	 * @throws SQLException
	 */
	public void execute() throws IOException, SQLException {
		statement.executeBatch();
	}

	private boolean checkStatementEnds(String s) {
		return (s.indexOf(QUERY_ENDS) != -1);
	}

	/**
	 * @return the statement
	 */
	public Statement getStatement() {
		return statement;
	}

	/**
	 * @param statement the statement to set
	 */
	public void setStatement(Statement statement) {
		this.statement = statement;
	}
	
}