# org.atria.dao #

org.atria.dao is a lightweight ORM (Object Relational Mapping) for SQLite. It allows an easy access to SQLite database via Java.

org.atria.dao uses annotation classes (@IEntity and @IField) for persisting models and attributes. The library uses the pattern "convention over configuration". Eg. if in @IEntity it isn't specified the attribute "name", the simple class name is used as a table name. Same rules are applied for @IField annotation.

DAO.Sqlite is a ready-to-use singleton class, which doesn't require any configurations. It can be connected to multiple SQLite databases (in memory and persisted) using the method 
```
#!java

DAO.Sqlite::use(String dbFile)
```
Whether the dbFile is null or a blank string database is in memory, without any persistence.

org.atria.dao automatically configures the database, creating SQL-Script for table and indexes at the first usage. To speed up the first-time execution, warm-up the application invoking 
```
#!java
DAO.Sqlite::createTableIfNotExist(Class<?> clazz) 
```
for each annotated class.

The following code defines a database entity called "person" with four fields, where id is an auto-increment serial and it is also the primary key. Field surname is also indexed field.

```
#!java

import java.util.Date;

import org.atria.dao.IEntity;
import org.atria.dao.IField;

@IEntity
public class Person {
	@IField(pk=true, serial=true)
	private long id;
	
	@IField
	private String name;
	
	@IField(index=true)
	private String surname;
	
	@IField
	private Date birthday;
        
    // getters and setters omitted
}
```

org.atria.dao.DAO class provides a wide range of methods to handle database entities, get them using org.atria.dao.Criteria<T>.

```
#!java

DAO.Sqlite.save(person);
```

# to be continued... #